import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
    justifyContent: 'center',
    flex: 1,
  },
  scroll: {
    marginVertical: 50,
  },
  text: {
    paddingHorizontal: 20,
    fontSize: 18,
  },
  backBtnText: {
    fontSize: 20,
  },
  backBtn: {
    marginTop: 50,
    marginLeft: 20,
  },
})
