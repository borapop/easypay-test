import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import { Actions } from 'react-native-router-flux'

import styles from './styles'

class Profile extends Component<{}> {
  render() {
    return(
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.backBtn}
          onPress={Actions.login}
        >
          <Text style={styles.backBtnText}>
            {'< Back'}
          </Text>
        </TouchableOpacity>
        <ScrollView style={styles.scroll}>
          <Text style={styles.text}>
            {this.props.jsonString}
          </Text>
        </ScrollView>
      </View>
    )
  }
}

export default Profile
