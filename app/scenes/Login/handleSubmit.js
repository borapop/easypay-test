import { Alert } from 'react-native'
import base64 from 'base-64'
import { Actions } from 'react-native-router-flux'

export function onSubmit(values, dispatch, props) {
  const auth = base64.encode('EP-df791b9d-12fa-4a17-aa06-8d7adde89bd7:EP-1510897744539-14c5cdb7-e5c9-4a80-95d7-5e43393a2305')
  const { username, password } = values
  fetch('https://api.easypay.co.id/EP-80deefcd-9769-4e81-9708-27d1031479e7/oauth2/token', {
    method: 'POST',
    headers: {
      'Authorization': 'Basic ' + auth,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      username,
      password,
      grant_type: 'password'
    })
  }).then(res => {
    res.json().then(json => {
      if (res.status !== 200) {
        Alert.alert('Error', JSON.stringify(json))
      } else {
        Actions.profile({ jsonString: JSON.stringify(json) })
      }
    })
  }).catch(error => {
    Alert.alert('Error', JSON.stringify(json))
  })
}
