import { StyleSheet, Dimensions } from 'react-native'

export default StyleSheet.create({
  container: {
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  input: {
    height: 45,
    backgroundColor: '#F9F9F9',
    paddingHorizontal: 10,
  },
  inputContainer: {
    marginHorizontal: 20,
    marginTop: 8,
    alignSelf: 'stretch',
  },
  logo: {
    resizeMode: 'contain',
    height: 130,
  },
  btn: {
    height: 50,
    marginHorizontal: 20,
    marginTop: 8,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2F7DB4'
  },
  btnText: {
    color: 'white',
    fontWeight: 'bold',
  },
  error: {
    color: 'red',
    marginLeft: 10,
    marginTop: 5,
  }
})
