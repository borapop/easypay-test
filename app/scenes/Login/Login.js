import React, { Component } from 'react'
import { Text, View, Image, KeyboardAvoidingView, TouchableOpacity } from 'react-native'
import { reduxForm, Field } from 'redux-form'

import { onSubmit } from './handleSubmit'
import CustomTextInput from '../../components/CustomTextInput'
import styles from './styles'

const validateUsername = (u) =>
  (u && u.length >= 3) ? undefined : 'At least 3 characters'

const validatePassword = (p) =>
  (p && p.length >= 6) ? undefined : 'At least 6 characters'

class Login extends Component<{}> {
  render() {
    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding"
      >
        <Image
          source={require('../../assets/logo.jpeg')}
          style={styles.logo}
        />
        <Field
          placeholder="Phone number"
          name="username"
          validate={validateUsername}
          component={CustomTextInput}
          containerStyle={styles.inputContainer}
          inputStyle={styles.input}
          errorStyle={styles.error}
        />
        <Field
          placeholder="Password"
          secureTextEntry
          name="password"
          validate={validatePassword}
          component={CustomTextInput}
          containerStyle={styles.inputContainer}
          inputStyle={styles.input}
          errorStyle={styles.error}
        />
        <TouchableOpacity
          style={styles.btn}
          onPress={this.props.handleSubmit}
        >
          <Text style={styles.btnText}>
            LOGIN
          </Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    )
  }
}

export default reduxForm({
  form: 'login',
  onSubmit,
})(Login)
