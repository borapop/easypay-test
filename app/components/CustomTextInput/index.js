import React from 'react'
import { Text, TextInput, View } from 'react-native'

const CustomTextInput = (props) => {
  const {
    inputStyle,
    containerStyle,
    children,
    input,
    meta,
    errorStyle,
    ...inputProps
  } = props;
  const { error, touched, visited } = meta
  return (
    <View style={containerStyle}>
      {children}
      <TextInput
        style={inputStyle}
        {...props}
        {...inputProps}
        onChangeText={input.onChange}
        onBlur={input.onBlur}
        onFocus={input.onFocus}
        value={input.value}
      />
      {touched && visited && error &&
        <Text style={errorStyle}>
          {error}
        </Text>
      }
    </View>
  );
}

export default CustomTextInput
