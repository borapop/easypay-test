/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native'
import {
  Scene,
  Router,
} from 'react-native-router-flux'
import { Provider, connect } from 'react-redux'

import Login from './app/scenes/Login'
import Profile from './app/scenes/Profile'
import configureStore from './configureStore'

const RouterWithRedux = connect()(Router)
const store = configureStore()


class EasyPayTest extends Component<{}> {
  render() {
    return (
      <Provider
        store={store}
      >
        <RouterWithRedux>
          <Scene key="root">
            <Scene
              hideNavBar
              key="login"
              initial
              component={Login}
            />
            <Scene
              hideNavBar
              key="profile"
              component={Profile}
            />
          </Scene>
        </RouterWithRedux>
      </Provider>
    )
  }
}

export default EasyPayTest
